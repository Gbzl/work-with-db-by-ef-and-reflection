﻿using DbEfAndReflectionTest.DTO;
using Microsoft.EntityFrameworkCore;

namespace DbEfAndReflectionTest;

public sealed class PostgresSqlDbContext : DbContext
{
    private readonly string _connectionString;
    private const int MAX_RETRY_COUNT = 3;
    private readonly TimeSpan MAX_RETRY_DELAY = TimeSpan.FromSeconds(3);

    public PostgresSqlDbContext(string connectionString)
    {
        _connectionString = connectionString;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseNpgsql(_connectionString, options => options.EnableRetryOnFailure(MAX_RETRY_COUNT, MAX_RETRY_DELAY, null));
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<UserDto>(entity =>
        {
            entity.ToTable("Users", "public");
            entity.Property(e => e.Id).HasColumnName("Id");
            entity.Property(e => e.FirstName).HasColumnName("FirstName");
            entity.Property(e => e.SecondName).HasColumnName("SecondName");
            entity.Property(e => e.Patronymic).HasColumnName("Patronymic");
            entity.Property(e => e.Email).HasColumnName("Email");
            entity.Property(e => e.DateBirth).HasColumnName("DateBirth");
        });
        modelBuilder.Entity<ProductDto>(entity =>
        {
            entity.ToTable("Products", "public");
            entity.Property(e => e.Id).HasColumnName("Id");
            entity.Property(e => e.Name).HasColumnName("Name");
            entity.Property(e => e.Description).HasColumnName("Description");
            entity.Property(e => e.Price).HasColumnName("Price");
            entity.Property(e => e.RemainingStock).HasColumnName("RemainingStock");
        });
        modelBuilder.Entity<OrdersDto>(entity =>
        {
            entity.ToTable("Orders", "public");
            entity.Property(e => e.Id).HasColumnName("Id");
            entity.Property(e => e.UserId).HasColumnName("UserId");
            entity.Property(e => e.DeliveryAddress).HasColumnName("DeliveryAddress");
            entity.Property(e => e.TotalAmount).HasColumnName("TotalAmount");
        });
        modelBuilder.Entity<ProductOrdersDto>(entity =>
        {
            entity.ToTable("ProductOrders", "public");
            entity.Property(e => e.Id).HasColumnName("Id");
            entity.Property(e => e.ProductId).HasColumnName("ProductId");
            entity.Property(e => e.OrderId).HasColumnName("OrderId");
        });
    }
}