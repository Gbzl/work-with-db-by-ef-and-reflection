﻿using DbEfAndReflectionTest.DTO;
using DbEfAndReflectionTest.Helpers;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Internal;

namespace DbEfAndReflectionTest;

static class Program
{
    static void Main(string[] args)
    {
        int nextStep = 0;
        string connectionString;
        bool canConnect;

        //connectionString = "Host=localhost;Port=5432;Database=DbHomeWorkOtus;Username=postgres;Password=1q2w3E4R";
        do
        {
            Console.WriteLine("Введите данные для подключения к СУБД Postgres");
            connectionString = $@"Host={ConsoleHelper.GetFiledFromConsole("postgres host")};";
            connectionString += $@"Port={ConsoleHelper.GetFiledFromConsole("postgres port")};";
            connectionString += $@"Database={ConsoleHelper.GetFiledFromConsole("postgres db name")};";
            connectionString += $@"Username={ConsoleHelper.GetFiledFromConsole("postgres username")};";
            connectionString += $@"Password={ConsoleHelper.GetFiledFromConsole("postgres password")};";
            Console.Clear();

            Console.WriteLine("Подключение к БД...");
            using var db = new PostgresSqlDbContext(connectionString);
            canConnect = db.Database.CanConnect();
            if (canConnect)
            {
                Console.WriteLine("Успешно");
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Не удалось подключится к БД, попробуйте еще раз");
            }
        } while (canConnect != true);
        Console.Clear();

        do
        {
            Console.WriteLine("\t 1. Пользователи");
            var users = DbRequests.BulkSelect<UserDto>(connectionString);
            Console.WriteLine(users.ToStringTable(PropertyHandler.GetPropertyArrName<UserDto>(), PropertyHandler.GetPropertiesValue<UserDto>()));

            Console.WriteLine();

            Console.WriteLine("\t 2. Заказы");
            var orderDtos = DbRequests.BulkSelect<OrdersDto>(connectionString);
            Console.WriteLine(orderDtos.ToStringTable(PropertyHandler.GetPropertyArrName<OrdersDto>(), PropertyHandler.GetPropertiesValue<OrdersDto>()));

            Console.WriteLine();

            Console.WriteLine("\t 3. Продукты");
            var productDto = DbRequests.BulkSelect<ProductDto>(connectionString);
            Console.WriteLine(productDto.ToStringTable(PropertyHandler.GetPropertyArrName<ProductDto>(), PropertyHandler.GetPropertiesValue<ProductDto>()));

            Console.WriteLine();

            Console.WriteLine("\t 4. Связующая таблица продуктов и заказов");
            var productOrdersDto = DbRequests.BulkSelect<ProductOrdersDto>(connectionString);
            Console.WriteLine(productOrdersDto.ToStringTable(PropertyHandler.GetPropertyArrName<ProductOrdersDto>(), PropertyHandler.GetPropertiesValue<ProductOrdersDto>()));

            bool parseResult;
            do
            {
                parseResult = int.TryParse(ConsoleHelper.GetFiledFromConsole("номер таблицы, чтобы вставить значение в таблицу \nДля выхода из приложения нажмите 0"), out nextStep);

                if (!parseResult)
                {
                    Console.WriteLine("Введенное значение не является числом");
                    continue;
                }

                switch (nextStep)
                {
                    case 0:
                        break;
                    case 1:
                        PropertyHandler.CollectAndInsertEntity<UserDto>(connectionString);
                        break;
                    case 2:
                        PropertyHandler.CollectAndInsertEntity<OrdersDto>(connectionString);
                        break;
                    case 3:
                        PropertyHandler.CollectAndInsertEntity<ProductDto>(connectionString);
                        break;
                    case 4:
                        PropertyHandler.CollectAndInsertEntity<ProductOrdersDto>(connectionString);
                        break;
                    default:
                        Console.WriteLine("Введенному значение не соответствует ни одна таблица");
                        parseResult = false;
                        break;
                }

            } while (parseResult != true);

            Console.Clear();
        } while (nextStep != 0);
    }
}