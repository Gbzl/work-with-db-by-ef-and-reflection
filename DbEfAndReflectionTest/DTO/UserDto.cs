﻿namespace DbEfAndReflectionTest.DTO;

public class UserDto
{
    public Guid Id { get; set; }
    public string FirstName { get; set; }
    public string SecondName { get; set; }
    public string? Patronymic { get; set; }
    public string Email { get; set; }
    public DateTime DateBirth { get; set; }
}