﻿namespace DbEfAndReflectionTest.DTO;

public class OrdersDto
{
    public Guid Id { get; set; }
    public Guid UserId { get; set; }
    public string DeliveryAddress { get; set; }
    public double TotalAmount { get; set; }
}