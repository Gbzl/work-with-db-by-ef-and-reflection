﻿namespace DbEfAndReflectionTest.DTO;

public class ProductOrdersDto
{
    public Guid Id { get; set; }
    public Guid ProductId { get; set; }
    public Guid OrderId { get; set; }
}