﻿namespace DbEfAndReflectionTest.Helpers;

public class ConsoleHelper
{
    public static string GetFiledFromConsole(string filedName)
    {
        string filed;
        do
        {
            Console.WriteLine($@"Введите {filedName}:");
            filed = Console.ReadLine()!;
        } while (string.IsNullOrEmpty(filed));

        return filed;
    }
}