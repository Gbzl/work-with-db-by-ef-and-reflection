﻿namespace DbEfAndReflectionTest.Helpers;

public class DbRequests
{
    public static void BulkInsert<T>(string connectionString, params T[] entityList) where T : class, new()
    {
        using var db = new PostgresSqlDbContext(connectionString);
        
        db.Set<T>().AddRange(entityList.ToList());
        db.SaveChanges();
    }

    public static List<T> BulkSelect<T>(string connectionString) where T : class, new()
    {
        using var db = new PostgresSqlDbContext(connectionString);
        return db.Set<T>().ToList();
    }
}