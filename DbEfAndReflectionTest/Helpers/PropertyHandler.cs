﻿namespace DbEfAndReflectionTest.Helpers;

public class PropertyHandler
{
    public static void CollectAndInsertEntity<T>(string connectionString) where T : class, new()
    {
        var entity = CollectEntity<T>();
        DbRequests.BulkInsert(connectionString, entity);
    }

    public static Func<T, object>[] GetPropertiesValue<T>() where T : class
    {
        return typeof(T).GetProperties().Select(x => new Func<T, object>(x.GetValue!)).ToArray(); 
    }
    
    public static string[] GetPropertyArrName<T>()
    {
        var propArr = new string[typeof(T).GetProperties().Length];
        for (var i = 0; i < typeof(T).GetProperties().Length; i++)
        {
            var prop = typeof(T).GetProperties()[i];
            propArr[i] = prop.Name;
        }

        return propArr;
    }

    private static T CollectEntity<T>() where T : class, new()
    {
        var entity = new T();
        var propertyName = GetPropertyArrName<T>();
        for (var i = 1; i < propertyName.Length; i++)
        {
            object castedValue = null;
            do
            {
                var value = ConsoleHelper.GetFiledFromConsole($"поле {propertyName[i]}");
                var valuePropertyInfo = typeof(T).GetProperty(propertyName[i]);
                castedValue = MasterCaster(valuePropertyInfo!.PropertyType, value);
                if (castedValue != null)
                    break;
                else
                    Console.WriteLine("Введеное значение не подходит по типу");
            } while (true);

            typeof(T).GetProperty(propertyName[i])!.SetValue(entity, castedValue);
        }

        return entity;
    }

    private static object MasterCaster(Type propertyInfo, string value)
    {
        if (propertyInfo == typeof(int))
        {
            if (int.TryParse(value, out var i))
                return i;
        }

        if (propertyInfo == typeof(double))
        {
            if (double.TryParse(value, out var d))
                return d;
        }

        if (propertyInfo == typeof(Guid))
        {
            if (Guid.TryParse(value, out var g))
                return g;
        }

        if (propertyInfo == typeof(DateTime))
        {
            if (DateTime.TryParse(value, out var dt))
                return dt.ToUniversalTime();
        }

        if (propertyInfo == typeof(string))
            return value;

        return null;
    }
}