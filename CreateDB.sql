CREATE TABLE "Users"
(
    "Id" UUID DEFAULT gen_random_uuid() PRIMARY KEY,
    "FirstName" TEXT NOT NULL,
    "SecondName" TEXT NOT NULL,
    "Patronymic" TEXT,
    "Email" TEXT NOT NULL,
    "DateBirth" TIMESTAMP NOT NULL
);

CREATE TABLE "Products"
(
    "Id" UUID DEFAULT gen_random_uuid() PRIMARY KEY,
    "Name" TEXT NOT NULL,
    "Description" TEXT NOT NULL,
    "Price" REAL NOT NULL,
    "RemainingStock" INTEGER NOT NULL
);

CREATE TABLE "Orders"
(
    "Id" UUID DEFAULT gen_random_uuid() PRIMARY KEY,
    "UserId" UUID NOT NULL,
    "DeliveryAddress" TEXT NOT NULL,
    "TotalAmount" REAL NOT NULL,
	FOREIGN KEY ("UserId") REFERENCES "Users" ("Id") ON DELETE CASCADE
); 

CREATE TABLE "ProductOrders"
(
    "Id" UUID DEFAULT gen_random_uuid() PRIMARY KEY,
	"ProductId" UUID NOT NULL,
	"OrderId" UUID NOT NULL,
	FOREIGN KEY ("ProductId") REFERENCES "Products" ("Id") ON DELETE CASCADE,
	FOREIGN KEY ("OrderId") REFERENCES "Orders" ("Id") ON DELETE CASCADE
);


INSERT INTO public."Users"("Id", "FirstName", "SecondName", "Patronymic", "Email", "DateBirth") VALUES ('{2852C09E-1003-41B6-BBD5-05DC5D2AD9C1}', 'Дмитрий', 'Дмитриев', 'Дмитриевич', 'Dima@dima.di', '1998-05-09');
INSERT INTO public."Users"("Id", "FirstName", "SecondName", "Patronymic", "Email", "DateBirth") VALUES ('{D81CE99B-DF8C-479E-9B36-D5BE8A953BF5}', 'Иван', 'Иванов', 'Иванович', 'Ivan@ivan.iv', '1991-05-09');
INSERT INTO public."Users"("Id", "FirstName", "SecondName", "Patronymic", "Email", "DateBirth") VALUES ('{9475F5DB-71C0-45FA-A494-FB00ACACE84F}', 'Мария', 'Марусевна', 'Мариевна', 'Mariya@mariya.ma', '1999-08-03');
INSERT INTO public."Users"("Id", "FirstName", "SecondName", "Patronymic", "Email", "DateBirth") VALUES ('{36222624-83E7-4BB6-8D31-BDA900D0D87C}', 'Ольга', 'Ольгевна', 'Ольевна', 'Olga@olga.ol', '1984-01-03');
INSERT INTO public."Users"("Id", "FirstName", "SecondName", "Patronymic", "Email", "DateBirth") VALUES ('{43B606DB-C8BA-4EB5-952A-2ABE1E10B699}', 'Евгений', 'Евгеньев', 'Евгеньевич', 'Evgen@evgen.ev', '1994-09-22');
INSERT INTO public."Users"("Id", "FirstName", "SecondName", "Patronymic", "Email", "DateBirth") VALUES ('{B090D168-DF0D-475B-8696-AC2D42A9E708}', 'Богдан', 'Богданов', 'Богданович', 'Bogdan@bogdan.bo', '2003-08-10');

INSERT INTO public."Orders"("Id", "UserId", "DeliveryAddress", "TotalAmount") VALUES ('{AC88A269-B331-45AD-A030-59BC0042BE61}', '{2852C09E-1003-41B6-BBD5-05DC5D2AD9C1}', 'Москва Улица Пушкина дом 3', '110');
INSERT INTO public."Orders"("Id", "UserId", "DeliveryAddress", "TotalAmount") VALUES ('{9302D2B2-09DA-42B0-A7B4-3FA1B6ABA562}', '{9475F5DB-71C0-45FA-A494-FB00ACACE84F}', 'СПб Улица Дмитриевская дом 4', '6000');
INSERT INTO public."Orders"("Id", "UserId", "DeliveryAddress", "TotalAmount") VALUES ('{74B88D43-C54F-4767-BDE0-BA3F4E8884B2}', '{2852C09E-1003-41B6-BBD5-05DC5D2AD9C1}', 'Москва Улица Пушкина дом 3', '900');
INSERT INTO public."Orders"("Id", "UserId", "DeliveryAddress", "TotalAmount") VALUES ('{941ACDD2-E8CD-43F2-9F13-2F8E917A8DA6}', '{B090D168-DF0D-475B-8696-AC2D42A9E708}', 'Тверь Улица Ленин дом 6', '1300');
INSERT INTO public."Orders"("Id", "UserId", "DeliveryAddress", "TotalAmount") VALUES ('{F75EE69E-AFEB-43BC-BCCE-ABD3E3BAC969}', '{43B606DB-C8BA-4EB5-952A-2ABE1E10B699}', 'Казань Улица Кекова дом 12', '1980');
INSERT INTO public."Orders"("Id", "UserId", "DeliveryAddress", "TotalAmount") VALUES ('{9732F028-9AD5-4B87-93A2-DB350BB6A780}', '{36222624-83E7-4BB6-8D31-BDA900D0D87C}', 'СПб Проспект Непокоренных дом 10', '2000');

INSERT INTO public."Products"("Id", "Name", "Description", "Price", "RemainingStock") VALUES ('{45A8CD70-961C-49B3-8A28-E7CAEDAC583B}', 'Чипсы Lays', 'Картофельный чипсы', '110', '40');
INSERT INTO public."Products"("Id", "Name", "Description", "Price", "RemainingStock") VALUES ('{E414547A-4F34-433B-83D6-D368CC029A3B}', 'Детское Мыло', 'Мыло для детей от 3х лет', '30', '50');
INSERT INTO public."Products"("Id", "Name", "Description", "Price", "RemainingStock") VALUES ('{758E89FB-D159-4FA8-97B5-FA85A5E0D102}', 'Колонки Logitech', 'Настольный колонки Logitech 3qx', '2300', '10');
INSERT INTO public."Products"("Id", "Name", "Description", "Price", "RemainingStock") VALUES ('{A813426E-FCFA-4384-A1D9-73EC06CBAB5E}', 'Рамка для фотографий', 'Деревянная рамка для фотографий 10х12', '900', '15');
INSERT INTO public."Products"("Id", "Name", "Description", "Price", "RemainingStock") VALUES ('{12377947-4A4F-4E2C-9C14-CEFF9AA8DBD1}', 'Очки солнцезащитные', 'Очки солнцезащитные в пластиковой оправе', '150', '30');

INSERT INTO public."ProductOrders"("Id", "ProductId", "OrderId") VALUES ('{E0B9E87A-7B38-4EE3-9B21-163BB2C59BF7}', '{45A8CD70-961C-49B3-8A28-E7CAEDAC583B}', '{AC88A269-B331-45AD-A030-59BC0042BE61}');
INSERT INTO public."ProductOrders"("Id", "ProductId", "OrderId") VALUES ('{1F949DFB-0E83-4B75-92AD-A4C0B6C327A1}', '{A813426E-FCFA-4384-A1D9-73EC06CBAB5E}', '{9302D2B2-09DA-42B0-A7B4-3FA1B6ABA562}');
INSERT INTO public."ProductOrders"("Id", "ProductId", "OrderId") VALUES ('{D34638F9-DB9E-41FB-9D06-D3E163FE411D}', '{12377947-4A4F-4E2C-9C14-CEFF9AA8DBD1}', '{9732F028-9AD5-4B87-93A2-DB350BB6A780}');
INSERT INTO public."ProductOrders"("Id", "ProductId", "OrderId") VALUES ('{24A1A076-1741-49BF-881A-A90F8E4B5566}', '{758E89FB-D159-4FA8-97B5-FA85A5E0D102}', '{F75EE69E-AFEB-43BC-BCCE-ABD3E3BAC969}');
INSERT INTO public."ProductOrders"("Id", "ProductId", "OrderId") VALUES ('{20D9B518-C25B-4200-8FEB-24F6C17524F3}', '{E414547A-4F34-433B-83D6-D368CC029A3B}', '{74B88D43-C54F-4767-BDE0-BA3F4E8884B2}');